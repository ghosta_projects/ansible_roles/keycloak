---

- name: Install required system dependencies
  ansible.builtin.apt:
    name: "{{ item }}"
    update_cache: true
  loop: "{{ keycloak_dependecies }}"
  register: install_dependencies_result
  until: install_dependencies_result is succeeded
  retries: 3
  delay: 15

- name: Install python dependencies
  ansible.builtin.pip:
    name: "{{ item }}"
    state: present
  loop: "{{ keycloak_pip_dependecies }}"

- name: Create keycloak system group
  ansible.builtin.group:
    name: "{{ keycloak_system_group }}"
    system: true
    state: present

- name: Create keycloak system user
  ansible.builtin.user:
    name: "{{ keycloak_system_user }}"
    system: true
    create_home: false
    group: "{{ keycloak_system_group }}"
    shell: "/usr/sbin/nologin"
    home: "{{ keycloak_home_dir }}"

- name: Create keycloak directories
  ansible.builtin.file:
    path: "{{ item }}"
    state: directory
    owner: "{{ keycloak_system_user }}"
    group: "{{ keycloak_system_group }}"
    mode: 0750
  loop:
    - "{{ keycloak_home_dir }}"
    - "{{ keycloak_certs_dir }}"

- name: Download keycloak archive
  ansible.builtin.get_url:
    url: "{{ keycloak_archive_url }}"
    dest: "/tmp/{{ keycloak_archive_name }}"
    checksum: "sha1:{{ keycloak_checksum }}"
    mode: 0660
  register: download_archive
  until: download_archive is succeeded

- name: Check keycloak directory exist
  ansible.builtin.stat:
    path: "{{ keycloak_home_dir }}/data/"
  register: keycloak_work_dir

- name: Unpack keycloak archive to home directory
  ansible.builtin.unarchive:
    src: "/tmp/{{ keycloak_archive_name }}"
    dest: "{{ keycloak_home_dir }}"
    remote_src: true
    owner: "{{ keycloak_system_user }}"
    group: "{{ keycloak_system_group }}"
    extra_opts: [--strip-components=1]
  when: not keycloak_work_dir.stat.exists
