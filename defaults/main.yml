---
# Keycloak distribution powered by quarkus (wildfly deprecated)
keycloak_version: 20.0.3
keycloak_checksum: 9598d39d75e4bfdee9ae8d58923c42005d4d4720

keycloak_archive_repo: "https://github.com/keycloak/keycloak/releases/download/{{ keycloak_version }}"
keycloak_archive_name: "keycloak-{{ keycloak_version }}.tar.gz"
keycloak_archive_url: "{{ keycloak_archive_repo }}/{{ keycloak_archive_name }}"

keycloak_java_package: openjdk-11-jdk
keycloak_dependecies:
  - tar
  - python3-pip
  - "{{ keycloak_java_package }}"

keycloak_pip_dependecies:
  - cryptography
  - netaddr

keycloak_system_user: keycloak
keycloak_system_group: keycloak

keycloak_base_dir: /opt/keycloak
keycloak_home_dir: "{{ keycloak_base_dir }}/keycloak-{{ keycloak_version }}"
keycloak_certs_dir: "{{ keycloak_home_dir }}/certs"
keycloak_templates_dir: "{{ role_path }}/templates"

# Keycloak can be started in two operating modes: development mode or production mode
keycloak_start_dev: true

keycloak_admin_user: admin
keycloak_admin_pass: password

####### Configuration ########

keycloak_base_http_url: "http://{{ keycloak_hostname }}:{{ keycloak_http_port }}"
keycloak_base_https_url: "https://{{ keycloak_hostname }}:{{ keycloak_https_port }}"
keycloak_base_url: "{{ keycloak_base_http_url if keycloak_http_enabled else keycloak_base_https_url }}"

# Database
keycloak_db: false
keycloak_db_vendor: postgres
keycloak_db_name: keycloak
keycloak_db_username: keycloak
keycloak_db_password: securedbkeycloakpassword
keycloak_db_url: "jdbc:postgresql://localhost:5432/{{ keycloak_db_name }}"

# Observability
keycloak_healthcheck: true
keycloak_healthcheck_url: "{{ keycloak_base_url }}/realms/master/.well-known/openid-configuration"
keycloak_metrics: false

# HTTP
# If keycloak_base_url is switched to false, then HTTPS mode will turn on
keycloak_http_enabled: true
keycloak_http_port: 8080

# HTTPS
# Will turned on if keycloak_http_enabled is false
keycloak_https_port: 8443
keycloak_certificate_key_type: RSA
keycloak_certificate_key_size: 4096
keycloak_certificate_country_name: RU
keycloak_certificate_organization_name: some_org
keycloak_certificate_email_address: jdoe@rambler.ru
keycloak_certificate_common_name: "DNS:{{ keycloak_hostname }}"
keycloak_certificate_provider: selfsigned

# Hostname
keycloak_hostname: localhost

# Proxy
# none, edge, reencrypt, passthrough
keycloak_proxy: none
keycloak_hostname_path: /auth
keycloak_http_relative_path: /

# High availability mode (more then one keycloak instance)
keycloak_ha_mode: false

# Logging
keycloak_log: file # console, file or GELF
keycloak_log_level: info # fatal, error, warn, info, debug, trace, all, off
keycloak_log_file: "{{ keycloak_home_dir }}/data/log/keycloak.log"
# from https://www.keycloak.org/server/logging
keycloak_log_filr_format: "%d{yyyy-MM-dd HH:mm:ss,SSS} %-5p [%c] (%t) %s%e%n"

# Realm
keycloak_realms: false
keycloak_auth_client_id: admin-cli
keycloak_realm_auth_url: "{{ keycloak_base_url }}"
keycloak_realm_name: new_realm
keycloak_realm_state: present
